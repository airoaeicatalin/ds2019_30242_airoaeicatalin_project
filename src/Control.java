
import java.awt.event.*;

public class Control {
	private MonitoredData design;
	private Test model;
	public Control(MonitoredData design,Test model){
		this.design=design;
		this.model=model;
		design.attachProg(new ProgListener());
		design.attachTest(new TestListener());
		design.attachPm(new PmListener());
		design.attachShow(new ShowListener());

	}
	class ProgListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String nume=design.getNume();
			model.adaugaMuncitor(nume, "Programator");
			
		}
	}
	class TestListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				String nume=design.getNume();
				model.adaugaMuncitor(nume, "Tester");
				
		}
	}
	class PmListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				
				String nume=design.getNume();
				model.adaugaMuncitor(nume, "Project Manager");	
		}
	}
	class ShowListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
					String rez=model.afisareAngajati();
					design.setResult(rez);
		}
	}
}
