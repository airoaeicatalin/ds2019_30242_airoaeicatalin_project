import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class MonitoredData extends JFrame {

	private JPanel mainPanel = new JPanel();
	private JTextField Nume = new JTextField();
	private JTextArea afisare = new JTextArea();


	
	private JButton programator = new JButton("Programator");
	private JButton tester = new JButton("Tester");
	private JButton pm = new JButton("PM");
	private JButton show = new JButton("Show");

	private JLabel nume = new JLabel("Nume");

	private void addComponents(){
		nume.setBounds(10,10,100,20);
		Nume.setBounds(150,10,100,20);
		programator.setBounds(10,50,100,20);
		tester.setBounds(120,50,100,20);
		pm.setBounds(230,50,100,20);
		show.setBounds(10, 100, 100, 20);
		afisare.setBounds(10, 150, 400, 400);
		
		mainPanel.add(nume);
		mainPanel.add(Nume);
		mainPanel.add(tester);
		mainPanel.add(pm);
		mainPanel.add(show);
		mainPanel.add(afisare);
		mainPanel.add(programator);
		
}
	
	private void jFrameSetup(){
		setSize(700,700);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public MonitoredData(){
		add(mainPanel);
		mainPanel.setLayout(null);
		addComponents();
		jFrameSetup();
		
	}

	
	public String getNume(){
		return Nume.getText();
	}
	public void attachProg(ActionListener a){
		programator.addActionListener(a);
	}
	public void attachTest(ActionListener a){
		tester.addActionListener(a);
	}
	public void attachPm(ActionListener a){
		pm.addActionListener(a);
	}
	public void attachShow(ActionListener a){
		show.addActionListener(a);
	}
	public void setResult(String rez){
	afisare.setText(rez);
	}
}
