import java.util.ArrayList;

public class Test {
	ArrayList<Operations> angajati = new ArrayList<>();
	public void adaugaMuncitor(String nume, String functie){
		Operations m=new Operations(nume, functie);
		angajati.add(m);
	}	
	public String afisareAngajati(){
		String rez="";
		for(Operations m : angajati){
			rez=rez+m.nume+m.functie+"\n";
		}
		return rez;
	}
}
